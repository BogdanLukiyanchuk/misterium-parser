Одноручный меч - Владение одноручным мечом
Двуручный меч - Владение двуручным мечом
Кинжал - Владение кинжалом
Нож - Владение ножом
Ударно-дробящее - Владение ударно-дробящим оружием
Копье - Владение копьем
Алебарда - Владение алебардой
Двойной глейв - Владение двойным глейвом
Боевой топор - Владение боевым топором
Коса - Владение косой
Посох - Владение посохом
Древковое дробящее - Владение древковым дробящим оружием
Наручные когти - Владение наручными когтями
Шпага - Владение шпагой
Хлыст - Владение хлыстом
Катары - Владение катарами
Катана - Владение катаной
Чакрам - Владение чакрамом
Длинный/короткий лук - Владение длинным/коротким луком
Арбалет - Владение арбалетом
Праща - Владение пращей
Метательные ножи - Владение метательными ножами
Сюрикены - Владение сюрикенами
Метательные топорики - Владение метательными топориками
Щит - Владение щитом
Огонь - Магия Огня
Вода - Магия Воды
Земля - Магия Земли
Воздух - Магия Воздуха
Природа - Магия Природы
Свет - Магия Света
Тьма - Магия Тьмы
Металл - Магия Металла
Жизнь - Магия Жизни
Смерть - Магия Смерти
Разум - Магия Разума
Кровь - Магия Крови
Пустота - Магия Пустоты

Люди - Человек
Светлые Эльфы - Светлый эльф
Темные Эльфы - Темный эльф
Ману Астар (Дроу) - Дроу
Полукровки - Полукровка
Архоны - Архон
Гномы - Гном
Вифрэи - Вифрэй
Наги - Нага
Орки - Орк
Реведанты - Реведант
Вампиры - Вампир
Назгулы - Назгул
