package parser;

import javafx.util.Pair;
import org.apache.log4j.Logger;
import parser.model.statistics.NonCombatSkillStatistics;
import parser.model.statistics.OrganizationStatistics;
import parser.model.statistics.SkillStatistics;
import parser.model.statistics.Statistics;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Output {
    private static final Logger logger = Logger.getLogger(Output.class);

    public static void combineFiles(Path output, Path... files) {
        try {
            new FileOutputStream(output.toFile()).getChannel().truncate(0).close();
        } catch (Exception e) {
            logger.fatal("Не удалось очистить файл результата\n"
                    + output.toAbsolutePath()
            );
        }
        List<InputStream> isList = new ArrayList<>();
        for (Path file : files) {
            try {
                InputStream is = Files.newInputStream(file);
                isList.add(is);
            } catch (IOException e) {
                logger.fatal("Не удалось открыть файл\n"
                        + file.toAbsolutePath()
                );
            }
        }

        try (SequenceInputStream is = new SequenceInputStream(Collections.enumeration(isList));
             OutputStream out = Files.newOutputStream(output)) {
            byte[] buffer = new byte[4096];
            int count = is.read(buffer);
            while (count > 0) {
                out.write(buffer, 0, count);
                count = is.read(buffer);
            }
        } catch (IOException e) {
            logger.fatal("Ошибка при копировании таблиц в единый файл\n"
                    + output.toAbsolutePath());
        }

    }

    public static void fillMainTable(Statistics statistics, Path output) {
        fillMainTable(statistics, output, false);
    }

    public static void fillMainTable(Statistics statistics, Path output, boolean append) {
        List<String> lines = null;
        try {
            Path table = Paths.get(ClassLoader.getSystemResource("output_table_formats/main_table_format.txt").toURI());
            lines = Files.readAllLines(table);
        } catch (URISyntaxException e) {
            logger.fatal("Файл формата главной таблицы не найден\n"
                    + "output_table_formats/main_table_format.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось открыть файл формата главной таблицы\n"
                    + "output_table_formats/main_table_format.txt"
            );
        }
        if (null == lines) {
            return;
        }
        String currentDate = new SimpleDateFormat("dd.MM.yyyy").format(new Date());

        //fill post header
        int i = 0;
        for (; !lines.get(i).contains("[table"); i++) {
            String line = lines.get(i);
            line = line.replaceAll("\\$currentDate", currentDate);
            line = line.replaceAll("\\$numberOfPlayers", String.valueOf(statistics.getNumberOfPlayers()));
            line = line.replaceAll("\\$numberOfMen",
                    String.valueOf(statistics.getNumberOfPlayersWithCertainParameterValue("Пол", "Мужской")));
            line = line.replaceAll("\\$numberOfWomen",
                    String.valueOf(statistics.getNumberOfPlayersWithCertainParameterValue("Пол", "Женский")));
            line = line.replaceAll("\\$numberOfWarriors", String.valueOf(statistics.getNumberOfWarriors()));
            line = line.replaceAll("\\$numberOfMages", String.valueOf(statistics.getNumberOfMages()));
            lines.set(i, line);
        }

        //load weapons, magics and races labels
        Map<String, String> labels = getSkillLabel();

        //fill table body
        Map<String, SkillStatistics> map = statistics.getStatisticsForMainTable();
        SkillStatistics skillStatistics = null;
        int position = 0;
        Pattern pattern = Pattern.compile("\\[[^/]*\\](.*?)\\[");
        for (; i < lines.size(); i++) {
            String line = lines.get(i);
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                String found = matcher.group(1);
                if (found.isEmpty()) {
                    continue;
                }

                if (skillStatistics == null) {
                    String label = labels.get(found);
                    skillStatistics = (label == null) ? map.get(found) : map.get(label);
                } else if (found.equals("0")) {
                    line = line.substring(0, matcher.start(1)) +
                            skillStatistics.getValue(position++) +
                            line.substring(matcher.start(1) + 1);
                    lines.set(i, line);
                } else {
                    String label = labels.get(found);
                    skillStatistics = (label == null) ? map.get(found) : map.get(label);
                    position = 0;
                }
            }


        }


        try {
            if (append) {
                Files.write(output, lines, StandardOpenOption.APPEND);
            } else {
                Files.write(output, lines);
            }
        } catch (IOException e) {
            logger.fatal("Не удалось записать главную таблицу в файл\n"
                    + output.toAbsolutePath()
            );
        }
    }

    public static Map<String, String> getSkillLabel() {
        HashMap<String, String> result = new HashMap<>();
        List<String> lines = null;
        try {
            Path labelMap = Paths.get(ClassLoader.getSystemResource("data/main_table_labels.txt").toURI());
            lines = Files.readAllLines(labelMap);
        } catch (URISyntaxException e) {
            logger.fatal("Не удалось найти файл соответствий оружия/магии и навыков\n"
                    + "data/main_table_labels.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось открыть файл соответствий оружия/магии и навыков\n"
                    + "data/main_table_labels.txt"
            );
        }
        if (null == lines) {
            return result;
        }
        result.putAll(
                lines.stream().filter(line -> line.contains(" - "))
                        .collect(Collectors.toMap(
                                line -> line.split(" - ")[0],
                                line -> line.split(" - ")[1])
                        )
        );
        return result;
    }

    public static void fillOtherDataTable(Statistics statistics, Path output) {
        fillOtherDataTable(statistics, output, false);
    }

    public static void fillOtherDataTable(Statistics statistics, Path output, boolean append) {
        List<String> lines = null;
        try {
            Path otherData = Paths.get(ClassLoader.getSystemResource("output_table_formats/other_data_format.txt").toURI());
            lines = Files.readAllLines(otherData);
        } catch (URISyntaxException e) {
            logger.fatal("Файл формата таблицы прочих технических данных не найден\n"
                    + "output_table_formats/other_data_format.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось считать данные из файла формата прочих технических данных\n"
                    + "output_table_formats/other_data_format.txt"
            );
        }
        if (null == lines) {
            return;
        }

        List<Pair<String, Integer>> techniques = statistics.getTechniquesStatistics();
        List<Pair<String, Integer>> additional = statistics.getAdditionalClassesStatistics();
        List<Pair<String, Integer>> specializations = statistics.getMagicSpecializationsStatistics();

        List<String> resultLines = new ArrayList<>();

        int i = 0;
        for (; !lines.get(i).contains("#repeat") && i < lines.size(); i++) {
            resultLines.add(lines.get(i));
        }
        if (i < lines.size()) {
            int start = i;
            int end = -1;
            for (i = start + 1; !lines.get(i).contains("#end") && i < lines.size(); i++) {
            }
            if (i < lines.size()) {
                end = i;
                int count = IntStream.of(techniques.size(), additional.size(), specializations.size())
                        .max().orElse(-1);
                for (i = 0; i < count; i++) {
                    for (int j = start + 1; j < end; j++) {
                        String line = lines.get(j);
                        line = line.replaceAll("\\$technique", i < techniques.size()
                                ? String.valueOf(techniques.get(i).getKey()) : "");
                        line = line.replaceAll("\\$techCount", i < techniques.size()
                                ? String.valueOf(techniques.get(i).getValue()) : "");
                        line = line.replaceAll("\\$additionalClass", i < additional.size()
                                ? String.valueOf(additional.get(i).getKey()) : "");
                        line = line.replaceAll("\\$addCount", i < additional.size()
                                ? String.valueOf(additional.get(i).getValue()) : "");
                        line = line.replaceAll("\\$magicSpecialization", i < specializations.size()
                                ? String.valueOf(specializations.get(i).getKey()) : "");
                        line = line.replaceAll("\\$specCount", i < specializations.size()
                                ? String.valueOf(specializations.get(i).getValue()) : "");
                        resultLines.add(line);
                    }
                }
                for (i = end + 1; i < lines.size(); i++) {
                    resultLines.add(lines.get(i));
                }
            }
        }
        try {
            if (append) {
                Files.write(output, resultLines, StandardOpenOption.APPEND);
            } else {
                Files.write(output, resultLines);
            }
        } catch (IOException e) {
            logger.fatal("Не удалось записать прочие технические данные в файл\n"
                    + output.toAbsolutePath()
            );
        }
    }

    public static void fillTypeSkillsTable(Statistics statistics, Path output) {
        fillTypeSkillsTable(statistics, output, false);
    }

    public static void fillTypeSkillsTable(Statistics statistics, Path output, boolean append) {
        List<String> lines = null;
        try {
            Path tableFormat = Paths.get(
                    ClassLoader.getSystemResource("output_table_formats/type_skills_format.txt").toURI());
            lines = Files.readAllLines(tableFormat);
        } catch (URISyntaxException e) {
            logger.fatal("Не удалось найти файл формата таблицы навыков по типу\n"
                    + "output_table_formats/type_skills_format.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось открыть файл формата таблицы навыков по типу\n"
                    + "output_table_formats/type_skills_format.txt"
            );
        }
        if (null == lines) {
            return;
        }
        List<String> resultLines = new ArrayList<>();

        int i = 0;
        for (; !lines.get(i).contains("#repeat") && i < lines.size(); i++) {
            resultLines.add(lines.get(i));
        }
        if (i < lines.size()) {
            int start = i;
            int end = -1;
            for (i = start + 1; !lines.get(i).contains("#end") && i < lines.size(); i++) {
            }
            if (i < lines.size()) {
                end = i;
                Map<String, List<NonCombatSkillStatistics>> typeSkillsStatistics
                        = statistics.getTypeNonCombatSkillStatistics();

                Pattern typePattern = Pattern.compile("\\[[^/]*\\](.*?:type)\\[");
                Pattern valuePattern = Pattern.compile("\\[[^/]*\\](.*?:value)\\[");


                Set<String> requiredSkills = new HashSet<>();
                for (int j = start + 1; j < end; j++) {
                    String line = lines.get(j);
                    Matcher typeMatcher = typePattern.matcher(line);
                    Matcher valueMatcher = valuePattern.matcher(line);
                    if (typeMatcher.find()) {
                        requiredSkills.add(typeMatcher.group(1).split(":type")[0]);
                    }
                    if (valueMatcher.find()) {
                        requiredSkills.add(valueMatcher.group(1).split(":value")[0]);
                    }
                }

                int count = requiredSkills.stream()
                        .map(skill -> typeSkillsStatistics.get(skill).size())
                        .max(Integer::compareTo)
                        .orElse(0);

                for (i = 0; i < count; i++) {
                    for (int j = start + 1; j < end; j++) {
                        String line = lines.get(j);
                        Matcher typeMatcher = typePattern.matcher(line);
                        Matcher valueMatcher = valuePattern.matcher(line);
                        if (typeMatcher.find()) {
                            final String group = typeMatcher.group(1);
                            String skill = group.split(":type")[0];
                            List<NonCombatSkillStatistics> list = typeSkillsStatistics.get(skill);
                            String replace = i < list.size() ? list.get(i).skill : "";
                            line = line.replaceAll(group, replace);
                        }
                        if (valueMatcher.find()) {
                            final String group = valueMatcher.group(1);
                            String skill = group.split(":value")[0];
                            List<NonCombatSkillStatistics> list = typeSkillsStatistics.get(skill);
                            String replace = i < list.size() ? String.valueOf(list.get(i).getTotal()) : "";
                            line = line.replaceAll(group, replace);
                        }
                        resultLines.add(line);
                    }
                }
                for (i = end + 1; i < lines.size(); i++) {
                    resultLines.add(lines.get(i));
                }
            }
        }

        try {
            if (append) {
                Files.write(output, resultLines, StandardOpenOption.APPEND);
            } else {
                Files.write(output, resultLines);
            }
        } catch (IOException e) {
            logger.fatal("Не удалось записать таблицу навыков по типу в файл\n"
                    + output.toAbsolutePath()
            );
        }
    }

    public static void fillBestMastersTable(Statistics statistics, Path output) {
        fillBestMastersTable(statistics, output, false);
    }

    public static void fillBestMastersTable(Statistics statistics, Path output, boolean append) {
        List<String> lines = null;
        try {
            Path otherData = Paths.get(
                    ClassLoader.getSystemResource("output_table_formats/best_masters_format.txt").toURI());
            lines = Files.readAllLines(otherData);
        } catch (URISyntaxException e) {
            logger.fatal("Не удалось найти файл формата таблицы лучших мастеров\n"
                    + "output_table_formats/best_masters_format.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось открыть файл формата таблицы лучших мастеров\n"
                    + "output_table_formats/best_masters_format.txt"
            );
        }
        if (null == lines) {
            return;
        }

        List<String> resultLines = new ArrayList<>();

        int i = 0;
        for (; !lines.get(i).contains("#repeat") && i < lines.size(); i++) {
            resultLines.add(lines.get(i));
        }
        if (i < lines.size()) {
            int start = i;
            int end = -1;
            for (i = start + 1; !lines.get(i).contains("#end") && i < lines.size(); i++) {
            }
            if (i < lines.size()) {
                List<NonCombatSkillStatistics> skillsStatistics = statistics.getNonCombatAdvancedSkillsStatistics();
                end = i;
                int count = skillsStatistics.size();
                for (i = 0; i < count; i++) {
                    for (int j = start + 1; j < end; j++) {
                        String line = lines.get(j);
                        line = line.replaceAll("\\$NonCombatSkill", skillsStatistics.get(i).skill);
                        line = line.replaceAll("\\$specialistCount", String.valueOf(skillsStatistics.get(i).getAdditional()));
                        line = line.replaceAll("\\$masterCount", String.valueOf(skillsStatistics.get(i).getMain()));
                        resultLines.add(line);
                    }
                }
                for (i = end + 1; i < lines.size(); i++) {
                    resultLines.add(lines.get(i));
                }
            }
        }

        try {
            if (append) {
                Files.write(output, resultLines, StandardOpenOption.APPEND);
            } else {
                Files.write(output, resultLines);
            }
        } catch (IOException e) {
            logger.fatal("Не удалось записать таблицу лучших мастеров в файл\n"
                    + output.toAbsolutePath()
            );
        }

    }

    public static void fillOrganizationsTable(Statistics statistics, Path output) {
        fillOrganizationsTable(statistics, output, false);
    }

    public static void fillOrganizationsTable(Statistics statistics, Path output, boolean append) {
        List<String> lines = null;
        try {
            Path tableFormat = Paths.get(
                    ClassLoader.getSystemResource("output_table_formats/organizations_format.txt").toURI());
            lines = Files.readAllLines(tableFormat);
        } catch (URISyntaxException e) {
            logger.fatal("Не удалось найти файл формата таблицы организаций\n"
                    + "output_table_formats/organizations_format.txt"
            );
        } catch (IOException e) {
            logger.fatal("Не удалось открыть файл формата таблицы организаций\n"
                    + "output_table_formats/organizations_format.txt"
            );
        }
        if (null == lines) {
            return;
        }
        List<String> resultLines = new ArrayList<>();

        int i = 0;
        for (; !lines.get(i).contains("#repeat") && i < lines.size(); i++) {
            resultLines.add(lines.get(i));
        }
        if (i < lines.size()) {
            int start = i;
            int end = -1;
            for (i = start + 1; !lines.get(i).contains("#end") && i < lines.size(); i++) {
            }
            if (i < lines.size()) {
                end = i;

                List<OrganizationStatistics> orgStatistics = statistics.getOrganizationsStatistics();

                Pattern namePattern = Pattern.compile("\\[[^/]*\\](.*?:name)\\[");
                Pattern countPattern = Pattern.compile("\\[[^/]*\\](.*?:count)\\[");

                int count = orgStatistics.size();

                for (i = 0; i < count; i++) {
                    for (int j = start + 1; j < end; j++) {
                        String line = lines.get(j);
                        Matcher nameMatcher = namePattern.matcher(line);
                        Matcher countMatcher = countPattern.matcher(line);
                        if (nameMatcher.find()) {
                            final String group = nameMatcher.group(1);
                            String replace = orgStatistics.get(i).skill;
                            line = line.replaceAll(group, replace);
                        }
                        if (countMatcher.find()) {
                            final String group = countMatcher.group(1);
                            String replace = String.valueOf(orgStatistics.get(i).getTotal());
                            line = line.replaceAll(group, replace);
                        }
                        resultLines.add(line);
                    }
                }
                for (i = end + 1; i < lines.size(); i++) {
                    resultLines.add(lines.get(i));
                }
            }
        }

        try {
            if (append) {
                Files.write(output, resultLines, StandardOpenOption.APPEND);
            } else {
                Files.write(output, resultLines);
            }
        } catch (IOException e) {
            logger.fatal("Не удалось записать таблицу организаций в файл\n"
                    + output.toAbsolutePath()
            );
        }
    }

    public static void insertOrganizationsUnderMainTable(Path main, Path organizations) {
        List<String> lines = new ArrayList<>();
        try (BufferedReader mainReader = Files.newBufferedReader(main);
             BufferedReader orgReader = Files.newBufferedReader(organizations)) {
            String stop = "[/spoiler]";
            String line;
            while (!stop.equals(line = mainReader.readLine())) {
                lines.add(line);
                if (line == null) {
                    logger.error("Не удалось найти место вставки таблицы организаций");
                    return;
                }
            }
            while (!((line = orgReader.readLine()) == null)) {
                lines.add(line);
            }
            lines.add(stop);
            while ((!((line = mainReader.readLine()) == null))) {
                lines.add(line);
            }
        } catch (IOException e) {
            logger.error("Ошибка чтения файла при попытке вставить таблицу организаций под главной таблицей");
            return;
        }
        try {
            Files.write(main, lines);
        } catch (IOException e) {
            logger.fatal("Ошибка записи файла главной таблицы после вставки таблицы организаций");
        }
    }

}
