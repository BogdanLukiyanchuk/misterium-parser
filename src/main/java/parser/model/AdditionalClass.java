package parser.model;

import java.util.Arrays;
import java.util.stream.Collectors;

public class AdditionalClass extends Skill {
    public final int level;
    private AdditionalClass(String name, int level) {
        super(format(name));
        this.level = level;
    }

    private static String format(String name) {
        String[] parts = name.split(" ");
        for (int i = 0; i < parts.length; i++) {
            if (parts[i].length() > 0) {
                parts[i] = parts[i].substring(0, 1).toUpperCase() + parts[i].substring(1);
            }
        }
        return Arrays.stream(parts).collect(Collectors.joining(" "));
    }

    public static AdditionalClass getAdditionalClass(String additionalClass) {
        String[] data = additionalClass.split(" - ", 2);
        String name = data[0].trim();
        int level = Integer.parseInt(data[1].toLowerCase().replace("уровень", "").trim());
        if (level <= 0) {
            throw new IllegalArgumentException();
        }
        return new AdditionalClass(name, level);
    }

    public int compareTo(AdditionalClass o) {
        if (this == o) {
            return 0;
        }
        return Integer.compare(this.level, o.level);
    }

    @Override
    public int compareTo(Skill o) {
        if (AdditionalClass.class != o.getClass()) {
            throw new IllegalArgumentException();
        }
        return compareTo((AdditionalClass) o);
    }
}
