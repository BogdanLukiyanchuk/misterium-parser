package parser.model;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharacterList {
/*
    String playerName;
    String characterName;

    String battleMastery;
    String battleStyle;

    List<String> uniqueAbilities;
    List<String> raceAbilities; //Шестое чувство - 1 уровень
*/

    private Map<String, String> parameters = new HashMap<>();

    public Map<String, String> getParameters() {
        return parameters;
    }

    Map<String, Integer> intParameters = new HashMap<>();

    private List<NonCombatSkill> nonCombatSkills = new ArrayList<>();

    public List<NonCombatSkill> getNonCombatSkills() {
        return nonCombatSkills;
    }

    private List<Weapon> weapons = new ArrayList<>();

    public List<Weapon> getWeapons() {
        return weapons;
    }

    private List<AdditionalClass> additionalClasses = new ArrayList<>();

    public List<AdditionalClass> getAdditionalClasses() {
        return additionalClasses;
    }

    private List<Technique> techniques = new ArrayList<>();

    public List<Technique> getTechniques() {
        return techniques;
    }

    CharacterClass characterClass = CharacterClass.DEFAULT;

    public void setCharacterClass(String name) {
        characterClass = CharacterClass.getClassByName(name);
    }

    public CharacterClass getCharacterClass() {
        return characterClass;
    }

    private List<Magic> magics = new ArrayList<>();

    public List<Magic> getMagics() {
        return magics;
    }


    public void addMagicSkill(String magic) throws NumberFormatException {
        magics.add(Magic.getMagic(magic));
    }

    public void addWeaponSkill(String skill) {
        weapons.add(Weapon.getWeapon(skill));
    }

    public void addAdditionalClass(String additionalClass) {
        additionalClasses.add(AdditionalClass.getAdditionalClass(additionalClass));
    }

    public void addTechniqueSkill(String skill) {
        techniques.add(Technique.getTechnique(skill));
    }

    public void addNonCombatSkill(String skill, boolean getAdvanced) {
        if (getAdvanced) {
            nonCombatSkills.add(NonCombatSkill.getNonCombatAdvancedSkill(skill));
        } else {
            nonCombatSkills.add(NonCombatSkill.getNonCombatSkill(skill));
        }
    }

    public void addParameter(String parameterInfo) {
        String[] data = parameterInfo.split(": ", 2);
        String name = data[0].trim();
        String value = data[1].trim();
        try {
            int intVal = Integer.parseInt(value.split(" ", 2)[0].trim());
            intParameters.put(name, intVal);
            return;
        } catch (NumberFormatException e) {
            //do nothing
        }
        parameters.put(name, value);
    }

}
