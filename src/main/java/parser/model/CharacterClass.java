package parser.model;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class CharacterClass extends Skill{
    private static final Logger logger = Logger.getLogger(CharacterClass.class);
    public static final CharacterClass DEFAULT = new CharacterClass("Житель", null, null);
    private static HashMap<String, CharacterClass> classByName = new HashMap<>();

    static {
        try {
            Path resource = Paths.get(ClassLoader.getSystemResource("data/classes.txt").toURI());
            try (BufferedReader br = Files.newBufferedReader(resource)) {
                Type[] currentType = new Type[1];
                br.lines().forEach(line -> {
                            if (line.contains(":")) {
                                currentType[0] = Type.getType(line.split(":")[0]);
                            } else {
                                if (line.isEmpty()) {
                                    return;
                                }
                                String data[] = line.split(" - ");
                                String name = data[0].trim();
                                String parentName = data.length > 1 ? data[1].trim() : null;
                                CharacterClass c = parentName != null ?
                                        new CharacterClass(name, getClassByName(parentName), currentType[0]) :
                                        new CharacterClass(name, null, currentType[0]);
                                classByName.put(name, c);
                            }
                        }
                );
            } catch (IOException e) {
                logger.fatal("Couldn't load file with classes\n"
                + "Program was terminated"
                );
                System.exit(1);
            }
        } catch (URISyntaxException e) {
            System.out.println("couldn't get path to data/classes.txt\n"
            + "Program was terminated"
            );
            System.exit(1);
        }

    }

    public static CharacterClass getClassByName(String name) {
        return classByName.get(name);
    }

    public static List<CharacterClass> getKnownClasses() {
        return new ArrayList<>(classByName.values());
    }

    public static Set<String> getKnownClassesNames(Type minimumLevel) {
        if (null == minimumLevel) {
            return classByName.keySet();
        }

        Set<String> result = new HashSet<>();
        for (Map.Entry<String, CharacterClass> entry : classByName.entrySet()) {
            if (entry.getValue().type.compareTo(minimumLevel) > 0) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    public final CharacterClass parent;
    public final Type type;

    public CharacterClass(String name, CharacterClass parent, Type type) {
        super(name);
        this.parent = parent;
        this.type = type;
    }

    public String getWay() {
        //Адепт/Солдат
        return type == Type.BASIC ? name : parent.getWay();
    }

    public boolean isMage() {
        if (type == null) {
            return false;
        }
        return "Адепт".equals(getWay());
    }

    public boolean isWarrior() {
        if (type == null) {
            return false;
        }
        return "Солдат".equals(getWay());
    }


    public String getAdvancedWay() {
//      Маг
//      Волшебник
//      Разведчик
//      Воин
        return type == Type.BASIC ? null : type == Type.ADVANCED ? name : parent.getAdvancedWay();
    }

    @Override
    public String toString() {
        return "" +
                '\'' + name + '\'' +
                " - " + type.getName() +
                ", (" + (parent != null ? parent.name : "") + ')'
                ;
    }

    public static boolean checkColor(Type type, String style) {
        if (style == null || !style.contains("color:")) {
            return false;
        }
        if (type == null) {
            return false;
        }
        String color = style.split("color:", 2)[1];

        return color.trim().startsWith(type.color);
    }

    public int compareTo(CharacterClass o) {
        if (type == null) {
            return o == null ? 0 : -1;
        } else if (o.type == null) {
            return 1;
        }
        return type.compareTo(o.type);
    }

    @Override
    public int compareTo(Skill o) {
        if (CharacterClass.class != o.getClass()) {
            throw new IllegalArgumentException();
        }
        return compareTo((CharacterClass) o);
    }

    public enum Type {
        BASIC("Основные", "lime"),
        ADVANCED("Продвинутые", "yellow"),
        ELITE("Элитные", "red"),
        LEGENDARY("Легендарные", "fuchsia");

        private final String name;
        private final String color;

        Type(String name, String color) {
            this.name = name;
            this.color = color;
        }

        public String getName() {
            return name;
        }

        public static Type getType(String name) {
            return Arrays.stream(Type.values()).filter(type -> name.equals(type.name)).findFirst().orElse(null);
        }
    }
}
