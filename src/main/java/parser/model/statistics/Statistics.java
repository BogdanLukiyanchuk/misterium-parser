package parser.model.statistics;

import javafx.util.Pair;

import java.util.*;

public interface Statistics {
    List<Pair<String, Integer>> getTechniquesStatistics();

    List<Pair<String, Integer>> getAdditionalClassesStatistics();

    List<AdditionalClassStatistics> getAddClassesStats();

    List<Pair<String, Integer>> getMagicSpecializationsStatistics();

    List<MagicSpecializationStatistics> getMagicSpecStats();

    MagicSkillStatistics getMagicSkillStatistics(String magicSkill);

    WeaponSkillStatistics getWeaponSkillStatistics(String weaponSkill);

    ClassStatistics getCharacterClassStatistics(String name);

    Map<String, List<NonCombatSkillStatistics>> getTypeNonCombatSkillStatistics();

    NonCombatSkillStatistics getSingleNonCombatAdvancedSkillStatistics(String skill);

    List<NonCombatSkillStatistics> getNonCombatAdvancedSkillsStatistics();

    int getNumberOfMages();

    int getNumberOfWarriors();

    int getNumberOfPlayers();

    int getNumberOfPlayersWithCertainParameterValue(String parameter, String value);

    Map<String, SkillStatistics> getStatisticsForMainTable();

    Set<String> getRaces();

    List<OrganizationStatistics> getOrganizationsStatistics();
}
