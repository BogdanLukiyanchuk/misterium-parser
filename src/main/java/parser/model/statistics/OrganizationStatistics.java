package parser.model.statistics;

public class OrganizationStatistics extends SkillStatistics {
    public OrganizationStatistics(String organization) {
        super(organization);
    }

    public OrganizationStatistics(String organization, int total) {
        this(organization);
        this.total = total;
    }
}
