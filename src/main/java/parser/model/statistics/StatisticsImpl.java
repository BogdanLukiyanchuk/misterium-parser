package parser.model.statistics;

import javafx.util.Pair;
import org.apache.log4j.Logger;
import parser.model.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StatisticsImpl implements Statistics {
    private static final Logger logger = Logger.getLogger(StatisticsImpl.class);
    private List<CharacterList> charactersLists;

    public StatisticsImpl(List<CharacterList> charactersLists) {
        this.charactersLists = charactersLists;
    }

    public StatisticsImpl() {
        load();
    }

    public void load() {
        this.charactersLists = new Provider().getCharactersLists();
        logger.info(charactersLists.size() + " Анкет загружено");
    }

    @Override
    public List<Pair<String, Integer>> getTechniquesStatistics() {

        final List<Pair<String, Integer>> result = new ArrayList<>();

        Map<String, Long> mainTechniques = charactersLists.stream()
                .flatMap(characterList -> characterList.getTechniques().stream())
                .map(technique -> technique.name).collect(
                        Collectors.groupingBy(
                                Function.identity(), Collectors.counting()
                        )
                );

        for (Map.Entry<String, Long> entry : mainTechniques.entrySet()) {
            result.add(new Pair<>(entry.getKey(), entry.getValue().intValue()));
        }
        result.sort((p1, p2) -> Integer.compare(p2.getValue(), p1.getValue()));

        return result;

    }

    @Override
    public List<Pair<String, Integer>> getAdditionalClassesStatistics() {
        List<Pair<String, Integer>> result = new ArrayList<>();
        HashMap<String, Integer> temp = new HashMap<>();
        for (CharacterList characterList : charactersLists) {
            for (AdditionalClass additionalClass : characterList.getAdditionalClasses()) {
                String name = additionalClass.name;
                if (name.contains("Дочь") || name.contains("Сын")) {
                    name = name.replaceAll("Дочь|Сын", "Дочь(Сын)");
                }
                temp.put(name, temp.getOrDefault(name, 0) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : temp.entrySet()) {
            Pair<String, Integer> pair = new Pair<>(entry.getKey(), entry.getValue());
            result.add(pair);
        }

        result.sort((p1, p2) -> Integer.compare(p2.getValue(), p1.getValue()));

        return result;
    }

    @Override
    public List<AdditionalClassStatistics> getAddClassesStats() {
        return charactersLists.stream()
                .flatMap(characterList -> characterList.getAdditionalClasses().stream())
                .collect(
                        Collector.of(
                                HashMap::new,
                                (HashMap<String, AdditionalClassStatistics> map, AdditionalClass additional) -> {
                                    String s = additional.name;
                                    if (s.contains("Дочь") || s.contains("Сын")) {
                                        s = s.replaceAll("Дочь|Сын", "Дочь(Сын)");
                                    }
                                    final String name = s;
                                    AdditionalClassStatistics found = map
                                            .computeIfAbsent(name, k -> new AdditionalClassStatistics(name));
                                    found.total++;
                                },
                                (map1, map2) -> {
                                    map2.entrySet().forEach(
                                            entry -> map1.merge(
                                                    entry.getKey(),
                                                    entry.getValue(),
                                                    (v1, v2) -> {
                                                        v1.total += v2.total;
                                                        return v1;
                                                    }
                                            )
                                    );
                                    return map1;
                                },
                                map -> map.values().stream().sorted().collect(Collectors.toList())
                        )
                );
    }

    @Override
    public List<Pair<String, Integer>> getMagicSpecializationsStatistics() {
        return charactersLists.stream()
                .flatMap(characterList -> characterList.getMagics().stream())
                .filter(Magic::hasSpecialization)
                .map(magic -> magic.specialization)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .map(entry -> new Pair<String, Integer>(entry.getKey(), entry.getValue().intValue()))
                .sorted((p1, p2) -> Integer.compare(p2.getValue(), p1.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public List<MagicSpecializationStatistics> getMagicSpecStats() {
        Map<String, Long> map = charactersLists.stream().flatMap(characterList -> characterList.getMagics().stream())
                .filter(Magic::hasSpecialization)
                .map(magic -> magic.specialization)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return map.entrySet().stream()
                .map(e -> new MagicSpecializationStatistics(e.getKey(), e.getValue().intValue()))
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public MagicSkillStatistics getMagicSkillStatistics(String magicSkill) {
        MagicSkillStatistics statistics = new MagicSkillStatistics(magicSkill);

        charactersLists.stream().filter(characterList -> !characterList.getMagics().isEmpty()).forEach(
                characterList -> {
                    List<Magic> magics = characterList.getMagics();
                    magics.sort(Collections.reverseOrder());

                    Magic best = magics.get(0);

                    Magic found = magics.stream()
                            .filter(magic -> magicSkill.equalsIgnoreCase(magic.name))
                            .findFirst().orElse(null);

                    if (null == found) {
                        return;
                    }

                    statistics.total++;
                    if (found.canBeMain() && found.compareTo(best) == 0) {
                        statistics.main++;
                    }
                }
        );

        return statistics;
    }

    @Override
    public WeaponSkillStatistics getWeaponSkillStatistics(String weaponSkill) {
        WeaponSkillStatistics statistics = new WeaponSkillStatistics(weaponSkill);

        charactersLists.stream().filter(characterList -> !characterList.getWeapons().isEmpty()).forEach(
                characterList -> {
                    List<Weapon> weapons = characterList.getWeapons();
                    weapons.sort(Collections.reverseOrder());

                    Weapon best = weapons.get(0);

                    Weapon found = weapons.stream()
                            .filter(weapon -> weaponSkill.equalsIgnoreCase(weapon.name))
                            .findFirst().orElse(null);

                    if (null == found) {
                        return;
                    }

                    statistics.total++;
                    if (found.canBeMain() && found.compareTo(best) == 0) {
                        statistics.main++;
                    }
                }
        );

        return statistics;
    }

    @Override
    public ClassStatistics getCharacterClassStatistics(String name) {
        ClassStatistics statistics = new ClassStatistics(name);

        statistics.total = (int) charactersLists.stream()
                .map(characterList -> characterList.getCharacterClass().name)
                .filter(name::equals)
                .count();

        return statistics;
    }

    public RaceStatistics getRaceStatistics(String race) {
        RaceStatistics statistics = new RaceStatistics(race);

        statistics.total = (int) charactersLists.stream()
                .filter(characterList -> race.equalsIgnoreCase(characterList.getParameters().get("Раса")))
                .count();

        return statistics;
    }

    @Override
    public Map<String, List<NonCombatSkillStatistics>> getTypeNonCombatSkillStatistics() {
        Map<String, List<NonCombatSkillStatistics>> result = new HashMap<>();
        charactersLists.stream()
                .flatMap(characterList -> characterList.getNonCombatSkills().stream())
                .collect(Collectors.groupingBy(
                        nonCombatSkill -> nonCombatSkill.name, Collectors.counting()
                )).entrySet().stream()
                .map(
                        entry -> new Pair<String, NonCombatSkillStatistics>(NonCombatSkill.nameTypes.get(entry.getKey()),
                                new NonCombatSkillStatistics(entry.getKey(), entry.getValue().intValue()))
                )
                .forEach(
                        entry -> {
                            List<NonCombatSkillStatistics> statList = result.getOrDefault(entry.getKey(), new ArrayList<>());
                            if (statList.isEmpty()) {
                                result.put(entry.getKey(), statList);
                            }
                            statList.add(entry.getValue());
                        }
                );

        result.values()
                .forEach(
                        list -> list.sort(SkillStatistics::compareTo)
                );

        return result;
    }

    @Override
    public NonCombatSkillStatistics getSingleNonCombatAdvancedSkillStatistics(String skill) {
        NonCombatSkillStatistics result = new NonCombatSkillStatistics(skill);

        charactersLists.stream()
                .flatMap(characterList -> characterList.getNonCombatSkills().stream())
                .filter(NonCombatSkill::isAdvanced)
                .forEach(nonCombatSkill -> {
                    result.total++;
                    if (nonCombatSkill.isMaster()) {
                        result.main++;
                    }
                });
        return result;
    }

    @Override
    public List<NonCombatSkillStatistics> getNonCombatAdvancedSkillsStatistics() {
        List<NonCombatSkillStatistics> result = new ArrayList<>();

        charactersLists.stream()
                .flatMap(characterList -> characterList.getNonCombatSkills().stream())
                .filter(NonCombatSkill::isAdvanced)
                .forEach(nonCombatSkill -> {
                    NonCombatSkillStatistics found = result.stream()
                            .filter(skillStatistics -> skillStatistics.skill.equals(nonCombatSkill.name))
                            .findFirst().orElse(null);
                    if (found == null) {
                        found = new NonCombatSkillStatistics(nonCombatSkill.name);
                        result.add(found);
                    }
                    found.total++;
                    if (nonCombatSkill.isMaster()) {
                        found.main++;
                    }
                });

        result.sort(SkillStatistics::compareTo);
        return result;
    }


    @Override
    public int getNumberOfMages() {
        return (int) charactersLists.stream()
                .map(CharacterList::getCharacterClass)
                .filter(CharacterClass::isMage)
                .count();
    }

    @Override
    public int getNumberOfWarriors() {
        return (int) charactersLists.stream()
                .map(CharacterList::getCharacterClass)
                .filter(CharacterClass::isWarrior)
                .count();
    }

    @Override
    public int getNumberOfPlayers() {
        return charactersLists.size();
    }

    @Override
    public int getNumberOfPlayersWithCertainParameterValue(String parameter, String value) {
        return (int) charactersLists.stream()
                .filter(characterList -> value.equalsIgnoreCase(characterList.getParameters().get(parameter)))
                .count();
    }

    @Override
    public HashMap<String, SkillStatistics> getStatisticsForMainTable() {
        HashMap<String, SkillStatistics> result = new HashMap<>();
        List<String> magics = charactersLists.stream()
                .flatMap(characterList -> characterList.getMagics().stream())
                .map(magic -> magic.name)
                .distinct()
                .collect(Collectors.toList());
        result.putAll(
                magics.stream().collect(
                        Collectors.toMap(Function.identity(), this::getMagicSkillStatistics)
                )
        );

        List<String> weapons = charactersLists.stream()
                .flatMap(characterList -> characterList.getWeapons().stream())
                .map(weapon -> weapon.name)
                .distinct()
                .collect(Collectors.toList());
        result.putAll(
                weapons.stream().collect(
                        Collectors.toMap(Function.identity(), this::getWeaponSkillStatistics)
                )
        );

        List<String> characterClasses = charactersLists.stream()
                .map(CharacterList::getCharacterClass)
                .map(characterClass -> characterClass.name)
                .distinct()
                .collect(Collectors.toList());
        result.putAll(
                characterClasses.stream().collect(
                        Collectors.toMap(Function.identity(), this::getCharacterClassStatistics)
                )
        );

        result.putAll(
                getRaces().stream().collect(
                        Collectors.toMap(Function.identity(), this::getRaceStatistics)
                )
        );

        return result;
    }

    @Override
    public Set<String> getRaces() {
        return charactersLists.stream()
                .filter(characterList -> characterList.getParameters().get("Раса") != null)
                .map(characterList -> characterList.getParameters().get("Раса"))
                .distinct()
                .collect(Collectors.toSet());
    }

    @Override
    public List<OrganizationStatistics> getOrganizationsStatistics() {
        return charactersLists.stream()
                .filter(characterList -> characterList.getParameters().get("Организация") != null)
                .map(characterList -> characterList.getParameters().get("Организация"))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .map(entry -> new OrganizationStatistics(entry.getKey(), entry.getValue().intValue()))
                .sorted()
                .collect(Collectors.toList());
    }

}
