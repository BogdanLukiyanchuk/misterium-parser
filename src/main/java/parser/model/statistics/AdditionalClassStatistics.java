package parser.model.statistics;

public class AdditionalClassStatistics extends SkillStatistics{
    public AdditionalClassStatistics(String skill) {
        super(skill);
    }

    @Override
    public int getMain() {
        return total;
    }

    @Override
    public int getAdditional() {
        return 0;
    }

    @Override
    public String toString() {
        return skill + ": " + getTotal();
    }
}
