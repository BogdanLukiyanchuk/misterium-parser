package parser.model.statistics;

public abstract class SkillStatistics implements Comparable<SkillStatistics> {

    public final String skill;
    int total = 0;
    int main = 0;

    public SkillStatistics(String skill) {
        this.skill = skill;
    }

    public int getTotal() {
        return total;
    }

    public int getMain() {
        return main;
    }

    public int getAdditional() {
        return total-main;
    }

    public Integer getValue(int position) {
        switch (position) {
            case 0:
                return getTotal();
            case 1:
                return getMain();
            case 2:
                return null;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public int compareTo(SkillStatistics o) {
        int result = Integer.compare(o.total, this.total);
        if (result != 0) {
            return result;
        }
        result = Integer.compare(o.main, this.main);
        if (result != 0) {
            return result;
        }
        return skill.compareTo(o.skill);
    }

    @Override
    public String toString() {
        return skill + ": " + total + " " + '(' + main + '/' + getAdditional()+')';
    }
}
