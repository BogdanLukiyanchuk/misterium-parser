package parser.model.statistics;

public class NonCombatSkillStatistics extends SkillStatistics{
    public NonCombatSkillStatistics(String skill) {
        super(skill);
    }

    public NonCombatSkillStatistics(String skill, int total) {
        this(skill);
        this.total = total;
    }
}
