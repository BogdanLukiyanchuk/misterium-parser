package parser.model.statistics;

public class MagicSpecializationStatistics extends SkillStatistics{
    public MagicSpecializationStatistics(String skill) {
        super(skill);
    }

    public MagicSpecializationStatistics(String skill, int total) {
        this(skill);
        this.total = total;
    }

    @Override
    public int getMain() {
        return total;
    }

    @Override
    public int getAdditional() {
        return 0;
    }

    @Override
    public String toString() {
        return skill + ": " + getTotal();
    }
}
