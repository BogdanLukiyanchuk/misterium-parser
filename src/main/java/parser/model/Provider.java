package parser.model;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeVisitor;
import parser.Main;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;


public class Provider {
    private static final Logger logger = Logger.getLogger(Provider.class);
    private static final String URL_FORMAT = "http://misterium-rpg.ru/viewtopic.php?id=%d";//4726, 4652 e.g.
    private static final String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";
    private static final String referer = "http://misterium-rpg.ru/viewforum.php?id=61";
    private static final int timeout = 5 * 1000;
    private static final int PARAMETERS_AND_NON_COMBAT_ADVANCED = 10;
    private static final int NON_COMBAT_COMMON = 11;
    private static final int COMBAT = 20;
    private static final int CLASSES = 30;
    private static final List<String> ignored;
    static {
        List<String> lines = null;
        try {
            Path ignored = Paths.get(ClassLoader.getSystemResource("data/ignored.txt").toURI());
            lines = Files.readAllLines(ignored);
        } catch (URISyntaxException e) {
            logger.warn("Файл не найден. Все строки учитываются\n"
                    + "data/ignored.txt"
            );
        } catch (IOException e) {
            logger.warn("Произошла ошибка при чтении файла\n"
                    + "data/ignored.txt"
            );
        }
        ignored = lines == null || lines.isEmpty() ? null
                : lines.stream().filter(s->!s.isEmpty()).filter(s -> !s.contains("#")).collect(Collectors.toList());
    }

    public final boolean shouldLoadFromDirectory;
    public final boolean shouldSaveFiles;

    public Provider() {
        this.shouldLoadFromDirectory = Boolean.parseBoolean(
                Main.properties.getProperty("shouldLoadFromDirectory", "false"));
        this.shouldSaveFiles = Boolean.parseBoolean(
                Main.properties.getProperty("shouldSaveFiles", "true"));
    }

    private Document getDocumentById(int id) throws IOException {
        String url = String.format(URL_FORMAT, id);
        return Jsoup.connect(url).userAgent(userAgent).referrer(referer).timeout(timeout).get();
    }

    private Document getDocumentByUrl(String url) throws IOException {
        logger.debug("Loading...\n"
                + url
        );
        return Jsoup.connect(url).userAgent(userAgent).referrer(referer).timeout(timeout).get();
    }

    private List<String> getTopicsUrls() {
        List<String> urls = new ArrayList<>();
        try {
            Document startPage = Jsoup.connect("http://misterium-rpg.ru/viewforum.php?id=61").userAgent(userAgent)
                    .referrer("http://misterium-rpg.ru/").timeout(timeout).get();
            Objects.requireNonNull(startPage);
            Document page = startPage;
            while (true) {
                Element table = page.select("table[summary=Список тем в форуме: Принятые анкеты]").first();
                if (null == table) {
                    break;
                }

                Elements links = table.getElementsContainingOwnText("Анкета");
                if (null == links) {
                    break;
                }

                for (Element link : links) {
                    urls.add(link.attr("href"));
                }

                Element pagelink = page.getElementsByClass("pagelink").first();

                Element nextPage = pagelink.getElementsContainingOwnText("»").first();
                if (nextPage == null) {
                    break;
                }
                page = Jsoup.connect(nextPage.attr("href")).userAgent(userAgent).referrer(referer).timeout(timeout).get();
            }
        } catch (IOException e) {
            logger.fatal("Ошибка при получении списка тем с анкетами", e);
        }
        return urls;
    }

    public List<CharacterList> getCharactersLists() {
        List<CharacterList> charactersLists = new ArrayList<>();

        if (shouldLoadFromDirectory) {
            Path dir = Paths.get("characters");
            try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(dir, "*.html")) {
                directoryStream.forEach(
                        path -> {
                            try {
                                String data = Files.lines(path).collect(Collectors.joining());
                                Document doc = Jsoup.parse(data);
                                Element block = doc.select("blockquote").first();
                                extractCharacterListFromBlock(charactersLists, block, path.toAbsolutePath().toString());
                            } catch (IOException e) {
                                logger.error("Couldn't get data from file\n"
                                        + path.toAbsolutePath());

                            }
                        }
                );
            } catch (IOException e) {
                logger.fatal("Couldn't load files from directory\n"
                        + dir.toAbsolutePath()
                );
            }
        } else {
            List<String> urls = getTopicsUrls();
            if (urls.isEmpty()) {
                logger.fatal("Links to Character Lists were not found\n"
                        + "The program was terminated"
                );
                System.exit(1);
            }

            for (String url : urls) {
                Document document = null;
                try {
                    document = getDocumentByUrl(url);
                } catch (IOException e) {
                    //do nothing
                }
                if (document == null) {
                    logger.fatal("Не удалось перейти по ссылке для получения анкеты\n"
                            + url
                    );
                    continue;
                }

                Elements posts = document.getElementsByClass("post-body");

                //todo also it's possible to get some information from 1st post
                //Element post1 = posts.first();

                boolean found = false;
                Element block = null;
                for (int i = 1; i < posts.size() && !found; i++) {
                    Element post = posts.get(i);
                    Elements blocks = post.select("blockquote");
                    block = blocks.first();

                    if (null == block) {
                        continue;
                    }

                    if (block.getElementsContainingText("Характеристики").size() > 0
                            && block.getElementsContainingText("Мирные умения").size() > 0
                            && block.getElementsContainingText("Боевые умения").size() > 0) {
                        found = true;
                    }
                    if (!found && blocks.size() > 1) {
                        for (int j = 1; j < blocks.size() && !found; j++) {
                            block = blocks.get(j);
                            if (block.getElementsContainingText("Характеристики").size() > 0
                                    && block.getElementsContainingText("Мирные умения").size() > 0
                                    && block.getElementsContainingText("Боевые умения").size() > 0) {
                                found = true;
                            }
                        }
                    }
                }

                if (!found) {
                    logger.error("Анкета не найдена\n"
                            + url);
                    continue;
                }

                if (shouldSaveFiles) {
                    saveToFile(url, block);
                }

                extractCharacterListFromBlock(charactersLists, block, url);

            }
        }
        return charactersLists;
    }

    private void extractCharacterListFromBlock(List<CharacterList> charactersLists, Element block, final String url) {
        CharacterList characterList = new CharacterList();

        Elements paragraphs = block.select("p");
        int part = PARAMETERS_AND_NON_COMBAT_ADVANCED;
        mainLoop:
        for (Element p : paragraphs) {
            switch (part) {
                case PARAMETERS_AND_NON_COMBAT_ADVANCED:
                    if (p.getElementsContainingOwnText("Мирные умения").isEmpty()) {
                        parseParameters(p, characterList, url);
                    } else {
                        part = NON_COMBAT_COMMON;
                        parseNonCombatAdvancedSkills(p, characterList, url);
                    }
                    break;

                case NON_COMBAT_COMMON:
                    if (p.getElementsContainingOwnText("Боевые умения").isEmpty()) {
                        parseNonCombatCommonSkills(p, characterList, url);
                    } else {
                        part = COMBAT;
                        parseAdditionalClasses(p, characterList, url);
                    }
                    break;

                case COMBAT:
                    if (!p.getElementsContainingText("Техника").isEmpty()) {
                        parseTechniques(p, characterList, url);
                    }

                    if (!p.getElementsContainingText("Владение").isEmpty()
                            || !p.getElementsContainingText("Рукопашный бой").isEmpty()
                            || !p.getElementsContainingText("Магия").isEmpty()) {
                        part = CLASSES;

                        parseWeaponAndMagic(p, characterList, url);
                    }
                    break;

                case CLASSES:
                    Set<String> possibleBetterClasses =
                            CharacterClass.getKnownClassesNames(characterList.characterClass.type);

                    if (possibleBetterClasses.isEmpty()) {
                        break mainLoop;//parsing finished
                    }

                    parseClasses(p, characterList, url, possibleBetterClasses);
                    break;
            }

/*            if (PARAMETERS_AND_NON_COMBAT_ADVANCED == part
                    && !p.getElementsContainingOwnText("Мирные умения").isEmpty()) {
                part = NON_COMBAT_COMMON;

                final StringBuilder sb = new StringBuilder();
                final boolean[] partDataFound = new boolean[]{false};
                final boolean[] skillFound = new boolean[]{false};
                NodeVisitor nodeVisitor = new NodeVisitor() {
                    public void head(Node node, int i) {
                        if (!partDataFound[0]) {
                            if (node.getClass() == TextNode.class) {
                                if (((TextNode) node).text().contains("Мирные умения")) {
                                    partDataFound[0] = true;
                                }
                            }
                        } else {
                            if (node.nodeName().equals("span") && node.attr("style").equals("color: maroon")) {
                                skillFound[0] = true;
                            }
                            if (skillFound[0]) {
                                if (node.getClass() == TextNode.class) {
                                    sb.append(((TextNode) node).text());
                                }
                                if (node.nodeName().equals("br")) {
                                    save();
                                }
                            }
                        }
                    }

                    public void tail(Node node, int i) {
                        if (node.nodeName().equals("p") && sb.length() > 0) {
                            save();
                        }
                    }

                    private void save() {
                        if (check(sb.toString())) {
                            try {
                                characterList.addNonCombatSkill(replaceBadChars(sb.toString()), true);
                            } catch (Exception e) {
                                logger.warn("Неверный формат продвинутого мирного навыка\n"
                                        + sb + "\n"
                                        + url
                                );
                            }
                        }
                        sb.setLength(0);
                        skillFound[0] = false;
                    }
                };

                p.traverse(nodeVisitor);

            } else if (NON_COMBAT_COMMON == part
                    && p.getElementsContainingOwnText("Боевые умения").isEmpty()) {
                final StringBuilder sb = new StringBuilder();
                NodeVisitor nodeVisitor = new NodeVisitor() {
                    @Override
                    public void head(Node node, int i) {
                        if (node.getClass() == TextNode.class) {
                            String text = ((TextNode) node).text();
                            sb.append(text);
                        }
                        if (node.nodeName().equals("br")) {
                            save();
                        }
                    }

                    @Override
                    public void tail(Node node, int i) {
                        if (node.nodeName().equals("p") && sb.length() > 0) {
                            save();
                        }
                    }

                    private void save() {
                        if (check(sb.toString())) {
                            try {
                                characterList.addNonCombatSkill(replaceBadChars(sb.toString()), false);
                            } catch (Exception e) {
                                logger.warn("Неверный формат мирного навыка\n"
                                        + sb + "\n"
                                        + url
                                );
                            }
                        }
                        sb.setLength(0);
                    }
                };

                p.traverse(nodeVisitor);

            } else if (part < COMBAT
                    && !p.getElementsContainingOwnText("Боевые умения").isEmpty()) {
                part = COMBAT;

                final StringBuilder sb = new StringBuilder();
                final boolean[] partDataFound = new boolean[]{false};
                final boolean[] skillFound = new boolean[]{false};
                NodeVisitor nodeVisitor = new NodeVisitor() {
                    public void head(Node node, int i) {
                        if (!partDataFound[0]) {
                            if (node.getClass() == TextNode.class) {
                                if (((TextNode) node).text().contains("Боевые умения")) {
                                    partDataFound[0] = true;
                                }
                            }
                        } else {
                            if (!skillFound[0]) {
                                if (node.getClass() == TextNode.class
                                        && ((TextNode) node).text().contains("Дополнительный класс")) {
                                    skillFound[0] = true;
                                }
                            } else {
                                if (node.getClass() == TextNode.class) {
                                    sb.append(((TextNode) node).text());
                                }
                                if (node.nodeName().equals("br")) {
                                    save();
                                }
                            }
                        }
                    }

                    public void tail(Node node, int i) {
                        if (node.nodeName().equals("p") && sb.length() > 0) {
                            save();
                        }
                    }

                    private void save() {
                        if (check(sb.toString())) {
                            try {
                                characterList.addAdditionalClass(replaceBadChars(sb.toString()));
                            } catch (Exception e) {
                                logger.warn("Неверный формат дополнительного класса\n"
                                        + sb + "\n"
                                        + url
                                );
                            }
                        }
                        sb.setLength(0);
                        skillFound[0] = false;
                    }
                };

                p.traverse(nodeVisitor);

            }

            if (part == COMBAT
                    && !p.getElementsContainingText("Техника").isEmpty()) {

                final StringBuilder sb = new StringBuilder();
                final boolean[] skillFound = new boolean[]{false};
                NodeVisitor nodeVisitor = new NodeVisitor() {
                    public void head(Node node, int i) {
                        if (!skillFound[0]) {
                            if (node.getClass() == TextNode.class
                                    && ((TextNode) node).text().contains("Техника")) {
                                skillFound[0] = true;
                                sb.append(((TextNode) node).text());
                            }
                        } else {
                            if (node.getClass() == TextNode.class) {
                                sb.append(((TextNode) node).text());
                            }
                            if (node.nodeName().equals("br")) {
                                save();
                            }
                        }
                    }

                    public void tail(Node node, int i) {
                        if (node.nodeName().equals("p") && sb.length() > 0) {
                            save();
                        }
                    }

                    private void save() {
                        if (check(sb.toString())) {
                            try {
                                characterList.addTechniqueSkill(replaceBadChars(sb.toString()));
                            } catch (Exception e) {
                                logger.warn("Неверный формат Техники боя\n"
                                        + sb + "\n"
                                        + url
                                );
                            }
                        }
                        sb.setLength(0);
                        skillFound[0] = false;

                    }
                };

                p.traverse(nodeVisitor);
            }

            if (part == COMBAT
                    && (!p.getElementsContainingText("Владение").isEmpty()
                    || !p.getElementsContainingText("Рукопашный бой").isEmpty()
                    || !p.getElementsContainingText("Магия").isEmpty())) {
                part = CLASSES;

                final StringBuilder sb = new StringBuilder();
                final boolean[] skillFound = new boolean[]{false};
                NodeVisitor nodeVisitor = new NodeVisitor() {
                    public void head(Node node, int i) {
                        if (!skillFound[0]) {
                            if (node.getClass() == TextNode.class) {
                                skillFound[0] = true;
                                String text = ((TextNode) node).text();
                                sb.append(text);
                            }
                        } else {
                            if (node.getClass() == TextNode.class) {
                                String text = ((TextNode) node).text();
                                if (!text.contains("Великая") && sb.toString().startsWith("Магия")) {
                                    if (node.parent().hasAttr("style")) {
                                        String style = node.parent().attr("style");
                                        if (style.contains("color:")) {
                                            String color = style.split("color:")[1].trim();
                                            text = " [color=" + color + ']' + text.trim() + "[/color] ";
                                        }
                                    }
                                }
                                sb.append(text);
                            }
                            if (node.nodeName().equals("br")) {
                                save();
                            }
                        }
                    }

                    public void tail(Node node, int i) {
                        if (node.nodeName().equals("p") && sb.length() > 0) {
                            save();
                        }
                    }

                    private void save() {
                        if (check(sb.toString())) {
                            try {
                                if (sb.toString().startsWith("Магия")) {
                                    characterList.addMagicSkill(replaceBadChars(sb.toString()));
                                } else if (sb.toString().startsWith("Владение")
                                        || sb.toString().startsWith("Рукопашный бой")) {
                                    characterList.addWeaponSkill(replaceBadChars(sb.toString()));
                                } else {
                                    logger.info("Подозрительная запись в разделе Магия и Владение оружием\n"
                                            + sb + "\n"
                                            + url
                                    );
                                }
                            } catch (Exception e) {
                                logger.warn("Неверный формат Магии или Владения оружием\n"
                                        + sb + "\n"
                                        + url
                                );
                            }
                        }
                        sb.setLength(0);
                        skillFound[0] = false;

                    }
                };

                p.traverse(nodeVisitor);

            }

            if (part == CLASSES) {
                Set<String> possibleBetterClasses =
                        CharacterClass.getKnownClassesNames(characterList.characterClass.type);
                if (!possibleBetterClasses.isEmpty()) {
                    NodeVisitor nodeVisitor = new NodeVisitor() {
                        @Override
                        public void head(Node node, int i) {
                            if (node.getClass() == TextNode.class) {
                                TextNode textNode = (TextNode) node;
                                if (check(textNode.text())) {
                                    final String text = replaceBadChars(textNode.text());
                                    if (possibleBetterClasses.contains(text)) {
                                        Node parent = textNode.parentNode();
                                        if ("span".equals(parent.nodeName())
                                                && CharacterClass.checkColor(CharacterClass.getClassByName(text).type, parent.attr("style"))) {
                                            characterList.setCharacterClass(text);
                                        } else {
                                            logger.warn("Проверьте, является ли поле классом:\n"
                                                    + text + "\n"
                                                    + url
                                            );
                                        }
                                    }
                                }
                            }
                        }

                        @Override
                        public void tail(Node node, int i) {

                        }
                    };

                    p.traverse(nodeVisitor);
                }
            }*/
        }

        charactersLists.add(characterList);
    }

    private void parseParameters(Element p, CharacterList characterList, String url) {
        final StringBuilder sb = new StringBuilder();
        NodeVisitor nodeVisitor = new NodeVisitor() {
            @Override
            public void head(Node node, int depth) {
                if (node.getClass() == TextNode.class) {
                    String text = ((TextNode) node).text();
                    sb.append(text);
                }
                if (node.nodeName().equals("br")) {
                    save();
                }
            }

            @Override
            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        String parameterInfo = replaceBadChars(sb.toString());
                        if (parameterInfo.contains(": ")) {
                            characterList.addParameter(parameterInfo);
                        }
                    } catch (Exception e) {
                        logger.warn("Неверный формат параметра персонажа\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseClasses(Element p, final CharacterList characterList, final String url, final Set<String> possibleBetterClasses) {
        NodeVisitor nodeVisitor = new NodeVisitor() {
            @Override
            public void head(Node node, int i) {
                if (node.getClass() == TextNode.class) {
                    TextNode textNode = (TextNode) node;
                    if (check(textNode.text())) {
                        final String text = replaceBadChars(textNode.text());
                        if (possibleBetterClasses.contains(text)) {
                            Node parent = textNode.parentNode();
                            if ("span".equals(parent.nodeName())
                                    && CharacterClass.checkColor(CharacterClass.getClassByName(text).type, parent.attr("style"))) {
                                characterList.setCharacterClass(text);
                            } else {
                                logger.warn("Проверьте, является ли поле классом:\n"
                                        + text + "\n"
                                        + url
                                );
                            }
                        }
                    }
                }
            }

            @Override
            public void tail(Node node, int i) {

            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseWeaponAndMagic(Element p, final CharacterList characterList, final String url) {
        final StringBuilder sb = new StringBuilder();
        final boolean[] skillFound = new boolean[]{false};
        NodeVisitor nodeVisitor = new NodeVisitor() {
            public void head(Node node, int i) {
                if (!skillFound[0]) {
                    if (node.getClass() == TextNode.class) {
                        skillFound[0] = true;
                        String text = ((TextNode) node).text();
                        sb.append(text);
                    }
                } else {
                    if (node.getClass() == TextNode.class) {
                        String text = ((TextNode) node).text();
                        if (!text.contains("Великая") && sb.toString().startsWith("Магия")) {
                            if (node.parent().hasAttr("style")) {
                                String style = node.parent().attr("style");
                                if (style.contains("color:")) {
                                    String color = style.split("color:")[1].trim();
                                    text = " [color=" + color + ']' + text.trim() + "[/color] ";
                                }
                            }
                        }
                        sb.append(text);
                    }
                    if (node.nodeName().equals("br")) {
                        save();
                    }
                }
            }

            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        if (sb.toString().startsWith("Магия")) {
                            characterList.addMagicSkill(replaceBadChars(sb.toString()));
                        } else if (sb.toString().startsWith("Владение")
                                || sb.toString().startsWith("Рукопашный бой")) {
                            characterList.addWeaponSkill(replaceBadChars(sb.toString()));
                        } else {
                            logger.info("Подозрительная запись в разделе Магия и Владение оружием\n"
                                    + sb + "\n"
                                    + url
                            );
                        }
                    } catch (Exception e) {
                        logger.warn("Неверный формат Магии или Владения оружием\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
                skillFound[0] = false;

            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseTechniques(Element p, final CharacterList characterList, final String url) {
        final StringBuilder sb = new StringBuilder();
        final boolean[] skillFound = new boolean[]{false};
        NodeVisitor nodeVisitor = new NodeVisitor() {
            public void head(Node node, int i) {
                if (!skillFound[0]) {
                    if (node.getClass() == TextNode.class
                            && ((TextNode) node).text().contains("Техника")) {
                        skillFound[0] = true;
                        sb.append(((TextNode) node).text());
                    }
                } else {
                    if (node.getClass() == TextNode.class) {
                        sb.append(((TextNode) node).text());
                    }
                    if (node.nodeName().equals("br")) {
                        save();
                    }
                }
            }

            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        characterList.addTechniqueSkill(replaceBadChars(sb.toString()));
                    } catch (Exception e) {
                        logger.warn("Неверный формат Техники боя\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
                skillFound[0] = false;

            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseAdditionalClasses(Element p, final CharacterList characterList, final String url) {
        final StringBuilder sb = new StringBuilder();
        final boolean[] partDataFound = new boolean[]{false};
        final boolean[] skillFound = new boolean[]{false};
        NodeVisitor nodeVisitor = new NodeVisitor() {
            public void head(Node node, int i) {
                if (!partDataFound[0]) {
                    if (node.getClass() == TextNode.class) {
                        if (((TextNode) node).text().contains("Боевые умения")) {
                            partDataFound[0] = true;
                        }
                    }
                } else {
                    if (!skillFound[0]) {
                        if (node.getClass() == TextNode.class
                                && ((TextNode) node).text().contains("Дополнительный класс")) {
                            skillFound[0] = true;
                        }
                    } else {
                        if (node.getClass() == TextNode.class) {
                            sb.append(((TextNode) node).text());
                        }
                        if (node.nodeName().equals("br")) {
                            save();
                        }
                    }
                }
            }

            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        characterList.addAdditionalClass(replaceBadChars(sb.toString()));
                    } catch (Exception e) {
                        logger.warn("Неверный формат дополнительного класса\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
                skillFound[0] = false;
            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseNonCombatCommonSkills(Element p, final CharacterList characterList, final String url) {
        final StringBuilder sb = new StringBuilder();
        NodeVisitor nodeVisitor = new NodeVisitor() {
            @Override
            public void head(Node node, int i) {
                if (node.getClass() == TextNode.class) {
                    String text = ((TextNode) node).text();
                    sb.append(text);
                }
                if (node.nodeName().equals("br")) {
                    save();
                }
            }

            @Override
            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        characterList.addNonCombatSkill(replaceBadChars(sb.toString()), false);
                    } catch (Exception e) {
                        logger.warn("Неверный формат мирного навыка\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
            }
        };

        p.traverse(nodeVisitor);
    }

    private void parseNonCombatAdvancedSkills(Element p, final CharacterList characterList, final String url) {
        final StringBuilder sb = new StringBuilder();
        final boolean[] partDataFound = new boolean[]{false};
        final boolean[] skillFound = new boolean[]{false};
        NodeVisitor nodeVisitor = new NodeVisitor() {
            public void head(Node node, int i) {
                if (!partDataFound[0]) {
                    if (node.getClass() == TextNode.class) {
                        if (((TextNode) node).text().contains("Мирные умения")) {
                            partDataFound[0] = true;
                        }
                    }
                } else {
                    if (node.nodeName().equals("span") && node.attr("style").equals("color: maroon")) {
                        skillFound[0] = true;
                    }
                    if (skillFound[0]) {
                        if (node.getClass() == TextNode.class) {
                            sb.append(((TextNode) node).text());
                        }
                        if (node.nodeName().equals("br")) {
                            save();
                        }
                    }
                }
            }

            public void tail(Node node, int i) {
                if (node.nodeName().equals("p") && sb.length() > 0) {
                    save();
                }
            }

            private void save() {
                if (check(sb.toString())) {
                    try {
                        characterList.addNonCombatSkill(replaceBadChars(sb.toString()), true);
                    } catch (Exception e) {
                        logger.warn("Неверный формат продвинутого мирного навыка\n"
                                + sb + "\n"
                                + url
                        );
                    }
                }
                sb.setLength(0);
                skillFound[0] = false;
            }
        };

        p.traverse(nodeVisitor);
    }

    private void saveToFile(String url, Element block) {
        url = url.trim();
        String name = url.substring(url.lastIndexOf("?id=") + 4) + ".html";
        final String folderName = "characters";
        Path folder = Paths.get(folderName);
        if (!Files.isDirectory(folder)) {
            try {
                Files.createDirectories(folder);
            } catch (IOException e) {
                logger.error("Не удалось создать директорию. Файлы не сохранены");
                return;
            }
        }
        Path file = folder.resolve(name);
        String fileUri = file.toUri().toString();

        Document document = Document.createShell(fileUri);
        document.body().append(block.outerHtml());

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(file)) {
            bufferedWriter.write(document.outerHtml());
        } catch (IOException e) {
            logger.error("Не удалось сохранить файл:\n"
                    + file
            );
        }

    }

    private static String replaceBadChars(String source) {
        return source.replace('–', '-').replaceAll("[\\s,\\u00a0]", " ").replaceAll("\\(.*?\\)", "").trim();
    }

    private static boolean check(String source) {
        if (ignored == null || ignored.isEmpty()) {
            return true;
        }
        return ignored.stream().noneMatch(source::contains);
    }
}
