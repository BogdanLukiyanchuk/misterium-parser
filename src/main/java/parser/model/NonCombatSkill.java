package parser.model;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NonCombatSkill extends Skill{
    public final String level;
    public final int mastery;
    public final String type;
    private final static int MASTERY_MIN = 0;
    private final static int MASTERY_MAX = 10;
    public static Map<String, String> nameTypes = new HashMap<>();
    static {
        loadTypes();
    }

    private static void loadTypes() {
        try {
            Path data = Paths.get(ClassLoader.getSystemResource("data/non_combat_skill_types.txt").toURI());
            List<String> lines = Files.readAllLines(data);
            String currentType = "";
            for (String line : lines) {
                if (!line.isEmpty()) {
                    if (line.contains(":")) {
                        currentType = line.split(":")[0];
                        continue;
                    }
                    String skill = line.trim();
                    nameTypes.put(skill, currentType);
                }
            }
        } catch (URISyntaxException | IOException e) {
            System.exit(1);
        }
    }

    public NonCombatSkill(String name, String level, int mastery) {
        super(name);
        if (level.isEmpty()) {
            throw new IllegalArgumentException();
        }
        String type = nameTypes.get(name);
        this.type = (type != null) ? type : "Общий";
        this.level = level;
        this.mastery = mastery;
    }

    public static NonCombatSkill getNonCombatAdvancedSkill(String skill) {
        String[] data = skill.split(" - ", 3);
        String name = data[0].trim();
        String level = data[1].trim();
        int mastery = Integer.parseInt(data[2].trim().split(" ", 2)[0]);
        if (mastery < MASTERY_MIN || mastery > MASTERY_MAX) {
            throw new IllegalArgumentException();
        }
        return new NonCombatSkill(name, level, mastery);
    }

    public static NonCombatSkill getNonCombatSkill(String skill) {
        String[] data = skill.split(" - ", 2);
        String name = data[0].trim();
        String level = data[1].trim();
        return new NonCombatSkill(name, level, 0);
    }

    public boolean isAdvanced() {
        return mastery > 0;
    }

    public boolean isMaster() {
        return mastery >= 4;
    }

    @Override
    public int compareTo(Skill o) {
        throw new UnsupportedOperationException();
    }
}
