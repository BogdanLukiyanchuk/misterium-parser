package parser.model;

import java.util.Objects;

public abstract class Skill implements Comparable<Skill> {
    public final String name;

    public Skill(String name) {
        Objects.requireNonNull(name);
        this.name = name.intern();
    }

    @Override
    public abstract int compareTo(Skill o);
}
