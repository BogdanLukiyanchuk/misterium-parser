package parser.model;

public class Technique extends Skill {
    final public int level;
    private static final int LEVEL_MAX = 7;
    private static final int LEVEL_MIN = 1;

    public Technique(String name, int level) {
        super(format(name));
        if (level < LEVEL_MIN || level > LEVEL_MAX) {
            throw new IllegalArgumentException("Неверный уровень техники");
        }
        this.level = level;
    }

    private static String format(String name) {
        try {
            return name.substring(name.indexOf('«'), name.lastIndexOf('»') +1);
        } catch (Exception e) {
            throw new IllegalArgumentException("Неверное имя техники");
        }
    }

    @Override
    public int compareTo(Skill o) {
        return 0;
    }

    public static Technique getTechnique(String info) {
        String[] data = info.split(" - ", 2);
        String name = data[0];
        int level = Integer.parseInt(data[1].split(" ")[0]);
        return new Technique(name, level);
    }


}
