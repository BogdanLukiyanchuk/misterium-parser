package parser.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Magic extends Skill{
    public final int level;
    public final String specialization;

    public Magic(String name, int level, String specialization) {
        super(magicFormat(name));
        this.level = level;
        this.specialization = specialization;
    }

    private static String magicFormat(String name) {
        String[] parts = name.split(" ");
        for (int i = 0; i < parts.length; i++) {
            if (parts[i].length() > 0) {
                parts[i] = parts[i].substring(0, 1).toUpperCase() + parts[i].substring(1);
            }
        }
        return Arrays.stream(parts).collect(Collectors.joining(" "));
    }

    static int getMagicLevel(String magicLevel) throws NumberFormatException {
//        Магия Воздуха - 1 порядок
//        Магия Земли - Геомант - 1 порядок
        magicLevel = magicLevel.trim().split(" ")[0];
        if ("Великая".equalsIgnoreCase(magicLevel)) {
            return 0;
        }
        return Integer.parseInt(magicLevel);
    }

    public static Magic getMagic(String magic) {
        int delimiterCount = StringUtils.countMatches(magic, " - ");
        if (delimiterCount == 0) {
            throw new IllegalArgumentException("Неверный формат магического скилла");
        }
        String data[] = magic.split(" - ", 3);
        String name = data[0];
        int level = getMagicLevel(delimiterCount == 1 ? data[1] : data[2]);
        String specialization = (delimiterCount == 1 ? null : data[1].trim());
        return new Magic(name, level, specialization);
    }

    public boolean hasSpecialization() {
        return specialization != null;
    }


    /*
    Main Magic should have level 4 or better.
     */
    public boolean canBeMain() {
        return level <= 4;
    }

    public int compareTo(Magic o) {
        if (this == o) {
            return 0;
        }
        int result = Integer.compare(o.level, this.level);
        if (result != 0) {
            return result;
        } else {
            if ((specialization == null) == (o.specialization == null)) {
                return 0;
            } else {
                return specialization == null ? -1 : 1;
            }
        }
    }

    @Override
    public int compareTo(Skill o) {
        if (Magic.class != o.getClass()) {
            throw new IllegalArgumentException();
        }
        return this.compareTo((Magic) o);
    }
}
