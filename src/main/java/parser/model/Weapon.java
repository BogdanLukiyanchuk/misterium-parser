package parser.model;

import java.util.Arrays;

public class Weapon extends Skill {
    public final WeaponSkill level;

    public Weapon(String name, WeaponSkill level) {
        super(weaponFormat(name));
        if (null == level) {
            throw new IllegalArgumentException();
        }
        this.level = level;
    }

    public Weapon(String name, String level) {
        this(name, WeaponSkill.getSkill(level));
    }

    public static Weapon getWeapon(String weaponSkillInfo) {
        String[] data = weaponSkillInfo.split(" - ", 2);
        return new Weapon(data[0], data[1]);
    }

    private static String weaponFormat(String name) {
        name = name.toLowerCase();
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    /*
    Main Weapon should have skill level Veteran or higher.
     */
    public boolean canBeMain() {
        return level.compareTo(WeaponSkill.VETERAN) >= 0;
    }

    public int compareTo(Weapon o) {
        if (this == o) {
            return 0;
        }
        return level.compareTo(o.level);
    }

    @Override
    public int compareTo(Skill o) {
        if (Weapon.class != o.getClass()) {
            throw new IllegalArgumentException();
        }
        return this.compareTo((Weapon) o);
    }


    public static enum WeaponSkill {
        NOVICE("Новичок"),
        STUDENT("Ученик"),
        EXPERIENCED("Опытный"),
        VETERAN("Ветеран"),
        MASTER("Мастер"),
        GRANDMASTER("Грандмастер");

        private final String name;

        WeaponSkill(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static WeaponSkill getSkill(String name) {
            return Arrays.stream(WeaponSkill.values()).filter(type -> name.equals(type.name)).findFirst().orElse(null);
        }

    }
}
