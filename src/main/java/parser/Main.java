package parser;

import org.apache.log4j.Logger;
import parser.model.statistics.Statistics;
import parser.model.statistics.StatisticsImpl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);

    public static final Properties properties = new Properties();

    static {
        try {
            Path resource = Paths.get(ClassLoader.getSystemResource("parser.properties").toURI());
            try (final InputStream is = Files.newInputStream(resource)) {
                properties.load(is);
            }
        } catch (URISyntaxException | IOException e) {
            logger.fatal("Couldn't load parser.properties file with program properties\n"
                    + "Program was terminated"
            );
        }
    }

    public static void main(String... args) throws IOException {
        logger.info("Программа запущена\n");

        String outputFolderName = properties.getProperty("outputFolder", "");
        Path outputPath = Paths.get(outputFolderName);
        if (!Files.isDirectory(outputPath)) {
            if (!Files.exists(outputPath)) {
                Files.createDirectories(outputPath);
            } else {
                logger.fatal("Couldn't open or create folder:\n"
                        + outputPath.toAbsolutePath() + "\n"
                        + "Program was terminated"
                );
                System.exit(1);
            }
        }

        Statistics statistics = new StatisticsImpl();
        Output.fillMainTable(statistics, outputPath.resolve("main_table.txt"));
        Output.fillOrganizationsTable(statistics, outputPath.resolve("organizations.txt"));
        Output.insertOrganizationsUnderMainTable(outputPath.resolve("main_table.txt"), outputPath.resolve("organizations.txt"));
        Output.fillOtherDataTable(statistics, outputPath.resolve("other_data_table.txt"));
        Output.fillBestMastersTable(statistics, outputPath.resolve("best_masters.txt"));
        Output.fillTypeSkillsTable(statistics, outputPath.resolve("peaceful_skills_by_type.txt"));
        Output.combineFiles(outputPath.resolve("result.txt"),
                outputPath.resolve("main_table.txt"),
                outputPath.resolve("other_data_table.txt"),
                outputPath.resolve("best_masters.txt"),
                outputPath.resolve("peaceful_skills_by_type.txt")
        );

        logger.info("Программа успешно завершила работу\n");
    }
}
